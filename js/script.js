$(document).ready(function(){
	$.ajax({
 		type: "GET",
 		url: "https://api.deezer.com/user/1567995002/charts/albums?output=jsonp",
 		dataType: "jsonp",
 		type: "application/json",
 		success: function(data){
 
 			for(i = 0; i < 5; i++){
 				if(data.data[i].cover_big !== null){
 					$('.deezer').append('<div class="a-music is-animated"><img src="' + data.data[i].cover_big + '"><div class="music-infos"><h5>By ' + data.data[i].artist.name + '</h5></div></div>');			
 				} else {
 					$('.deezer').append('<div class="a-music is-animated"><h2>Pochette non trouvée</h2></p><h5>' + data.data[i].artist.name + '</h5></div>');	
 				}

 			}
 		},
 		error: function(){
 			console.log("Erreur lors de la recuperation des resultats");
 		}
 	});
});